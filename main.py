import re
import datetime
import hashlib
import hmac
import json
import os
import requests
from pytz import timezone
from threading import Thread

from google.cloud import firestore
from flask import Flask, request, jsonify
import pandas as pd

app = Flask(__name__)

db = firestore.Client()
WORKOUTS_COLLECTION = db.collection('workouts')
CHALLENGE_COLLECTION = db.collection('challenges')

SLACK_TOKEN = os.environ.get('SLACK_TOKEN')


def chunks(l, n):
    """
    Divide list `l` into chunks of size `n` and yield chunks
    """
    for i in range(0, len(l), n):
        yield l[i:i + n]


# Python 3+ version of
# https://github.com/slackapi/python-slack-events-api/blob/master/slackeventsapi/server.py
def verify_signature(request):
    timestamp = request.headers.get('X-Slack-Request-Timestamp', '')
    signature = request.headers.get('X-Slack-Signature', '')

    req = str.encode('v0:{}:'.format(timestamp)) + request.get_data()
    request_digest = hmac.new(
        str.encode(os.environ['SLACK_SECRET']),
        req, hashlib.sha256
    ).hexdigest()
    request_hash = 'v0={}'.format(request_digest)

    if not hmac.compare_digest(request_hash, signature):
        raise ValueError('Invalid request/credentials.')


def add_workouts(workouts):
    ids = []
    for workout in workouts:
        _, workout = WORKOUTS_COLLECTION.add(workout)
        ids.append(workout.id)
    return ids


def get_workouts(user_name):
    query = WORKOUTS_COLLECTION.where('user_name', '==', user_name)
    query = query.order_by('date', direction=firestore.Query.DESCENDING)
    query = query.limit(1)
    results = query.stream()
    results = [res.to_dict() for res in results]
    return results
    

def delete_workouts(ids):
    for id_ in ids:
        WORKOUTS_COLLECTION.document(id_).delete()


def start_challenge(commands):
    """Start a challenge by adding it into the database

    expected commands: name workout_type t1 <username> <username> t2 <username> <username>

    """
    name = commands.pop(0)
    workout_type = commands.pop(0)
    users = []
    team = 0
    while commands:
        arg = commands.pop(0)
        if re.match(r't[0-9]', arg):
            team += 1
        else:
            users.append({
                'user_name': arg,
                'team': team
            })
    CHALLENGE_COLLECTION.add({
        'name': name,
        'workout_type': workout_type,
        'users': users,
        'started': datetime.datetime.now(timezone('US/Eastern'))
    })

def get_week_max(df):
    idx = df['value'].idxmax()
    user_name = df.iloc[idx]['user_name']
    value = df.iloc[idx]['value'].astype(int)
    date = df.iloc[idx]['date']
    return {
        'user_name': user_name,
        'value': value,
        'date': date
    }

def get_week_mvp(df):
    cum_value = df.groupby('user_name').sum()['value']
    user_name = cum_value.idxmax()
    value = cum_value[user_name]
    return {
        'user_name': user_name,
        'value': value
    }

def get_challenge(commands):
    """Get current standing for the user provided challenge name

    expected commands: challenge_name
    returns: string message formatted to show in slack
    """
    challenge_name = commands.pop(0)
    challenge = CHALLENGE_COLLECTION.where('name', '==', challenge_name).limit(1).stream()
    challenge = [res.to_dict() for res in challenge]
    if len(challenge) == 0:
        return 'No challenge with name {}'.format(challenge_name)
    challenge = challenge[0]
    workout_type = challenge['workout_type']
    challenge_started = challenge['started']
    today = datetime.datetime.now(timezone('US/Eastern'))
    today -= datetime.timedelta(hours=today.hour, seconds=today.second+today.minute*60, microseconds=today.microsecond)
    if today.weekday() == 0:
        week_start = max(challenge_started, today - datetime.timedelta(days=7))
    else:
        week_start = max(challenge_started, today - datetime.timedelta(days=today.weekday()))
    if today.day == 1:
        first_of_last_month = datetime.datetime(year=today.year, month=today.month-1, day=today.day, tzinfo=timezone('US/Eastern'))
        month_start = max(challenge_started, first_of_last_month)
    else:
        month_start = max(challenge_started, today - datetime.timedelta(days=today.day-1))
    week_workouts = WORKOUTS_COLLECTION.where('type', '==', workout_type).where('date', '>', week_start)
    month_workouts = WORKOUTS_COLLECTION.where('type', '==', workout_type).where('date', '>', month_start)
    week_workouts = [res.to_dict() for res in week_workouts.stream()]
    month_workouts = [res.to_dict() for res in month_workouts.stream()]
    week_df = pd.DataFrame(week_workouts)
    week_df['value'] = week_df['value'].astype(float)
    month_df = pd.DataFrame(month_workouts)
    month_df['value'] = month_df['value'].astype(float)
    users = challenge.get('users')
    num_teams = max([user['team'] for user in users])
    week_team_results = {n: {'user_names': [], 'count': 0} for n in range(1, num_teams+1)}
    month_team_results = {n: {'user_names': [], 'count': 0} for n in range(1, num_teams+1)}
    for user in users:
        user_name = user['user_name']
        team = user['team']
        week_team_results[team]['count'] += week_df[week_df['user_name']==user_name]['value'].sum()
        week_team_results[team]['user_names'].append(user_name)
        month_team_results[team]['count'] += month_df[month_df['user_name']==user_name]['value'].sum()
        month_team_results[team]['user_names'].append(user_name)
    resp_str = 'Standings this week:\n'+'\n'.join([
        'Team {}: {} {} ({})'.format(k, v['count'], workout_type, ', '.join(v['user_names']),)
        for k, v in week_team_results.items()
    ])
    resp_str += '\n\n'
    resp_str += 'Standings this month:\n'+'\n'.join([
        'Team {}: {} {} ({})'.format(k, v['count'], workout_type, ', '.join(v['user_names']),)
        for k, v in month_team_results.items()
    ])
    week_max = get_week_max(week_df)
    week_mvp = get_week_mvp(week_df)
    resp_str += '\n\n'
    resp_str += 'The MVP of the week was: {} with {} {}\n'.format(week_mvp['user_name'], week_mvp['value'], workout_type)
    resp_str += 'The best individual performance of the week was: {} with {} {} on {}\n'.format(week_max['user_name'], week_max['value'], workout_type, week_max['date'].date())
    return resp_str


def get_challenge_async(commands, response_url):
    resp_str = get_challenge(commands)
    payload = {
        'response_type': 'in_channel',
        'text': resp_str
    }
    requests.post(response_url, data=json.dumps(payload))


def send_slack_message(channel, message):
    url = 'https://slack.com/api/chat.postMessage'
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer {}'.format(SLACK_TOKEN)
    }
    data = {
        'channel': channel,
        'text': message
    }
    resp = requests.post(url, headers=headers, data=json.dumps(data))


def parse_args(commands, user_name):
    date = None
    workouts = []
    for arg, value in chunks(commands, 2):
        if arg == 'date':
            date = datetime.datetime.strptime(value, '%Y-%m-%d') + datetime.timedelta(hours=12)  # workaround to fix TZ bug
            continue
        workouts.append({
            'user_name': user_name,
            'type': arg,
            'value': value
        })
    if not date:
        date = datetime.datetime.now(timezone('US/Eastern'))
    for workout in workouts:
        workout['date'] = date
    return workouts


@app.route('/ping/')
def ping():
    return 'pong'


@app.route('/challenge/', methods=['GET'])
def send_challenge_standings_to_slack():
    challenge_name = request.args.get('name')
    channel = request.args.get('channel', '#fitness')
    challenge_standings_str = get_challenge([challenge_name])
    send_slack_message(channel, challenge_standings_str)
    return jsonify({
        'ok': True
    })


@app.route('/', methods=['POST'])
def main():
    if request.method != 'POST':
        return 'Only POST requests are accepted', 405

    verify_signature(request)

    text = request.form.get('text')
    user_name = request.form.get('user_name')
    response_url = request.form.get("response_url")

    commands = text.split()
    action = commands.pop(0)

    if action == 'add':
        workouts = parse_args(commands, user_name)
        workout_ids = add_workouts(workouts)
        return jsonify({
            'text': 'Successfully added workouts: {}'.format(', '.join(workout_ids)),
            'response_type': 'in_channel',
        })

    elif action == 'get':
        if len(commands) == 1:
            user_name = commands[0]
        results = get_workouts(user_name)
        if len(results) == 0:
            return jsonify({
                'response_type': 'in_channel',
                'text': 'No workouts found for {}!'.format(user_name)
            })
        else:
            res = results[0]
            return jsonify({
                'response_type': 'in_channel',
                'text': 'Last workout for {user_name} was a '
                        '{type} ({value}) on {date}'.format(**res)
            })

    elif action == 'delete':
        ids = commands
        delete_workouts(ids)
        id_strs = ', '.join(ids)
        return jsonify({
            'response_type': 'in_channel',
            'text': 'Deleted workouts with id(s): {}'.format(id_strs)
        })

    elif action == 'challenge':
        challenge_action = commands.pop(0)
        if challenge_action == 'start':
            start_challenge(commands)
            return jsonify({
                'response_type': 'in_channel',
                'text': 'Challenge started!'
            })
        elif challenge_action == 'get':
            thr = Thread(target=get_challenge_async, args=[commands, response_url])
            thr.start()
            return jsonify({
                'response_type': 'in_channel',
                'text': 'Querying challenge standings...standy by...'
            })
        # elif challenge_action == 'stop':
        #     stop_challenge(commands)
        else:
            return 'unsupported challenge action!'

    else:
        return 'must provide action'


if __name__ == '__main__':
    # This is used when running locally only. When deploying to Google App
    # Engine, a webserver process such as Gunicorn will serve the app. This
    # can be configured by adding an `entrypoint` to app.yaml.
    # Flask's development server will automatically serve static files in
    # the "static" directory. See:
    # http://flask.pocoo.org/docs/1.0/quickstart/#static-files. Once deployed,
    # App Engine itself will serve those files as configured in app.yaml.
    app.run(host='127.0.0.1', port=8080, debug=True)
